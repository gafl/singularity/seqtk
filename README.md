# seqtk Singularity container
### Bionformatics package seqtk<br>
Seqtk is a fast and lightweight tool for processing sequences in the FASTA or FASTQ format. It seamlessly parses both FASTA and FASTQ files which can also be optionally compressed by gzip.<br>
seqtk Version: 1.3<br>
[https://github.com/lh3/seqtk]

Singularity container based on the recipe: Singularity.seqtk_v1.3

Package installation using Miniconda3-4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build seqtk_v1.3.sif Singularity.seqtk_v1.3`

### Get image help
`singularity run-help ./seqtk_v1.3.sif`

#### Default runscript: STAR
#### Usage:
  `seqtk_v1.3.sif --help`<br>
    or:<br>
  `singularity exec seqtk_v1.3.sif seqtk --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull seqtk_v1.3.sif oras://registry.forgemia.inra.fr/gafl/singularity/seqtk/seqtk:latest`


